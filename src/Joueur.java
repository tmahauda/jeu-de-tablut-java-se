package jeu;
import plateau.*;
import jeu.*;

/**
* Cette classe est la classe Joueur
*
* Ecrit par Kévin Migadel
* @author Kévin Migadel
* @version 1.0
**/

public class Joueur {
	private String pseudo;
	private String camp;
	private int score;
	private boolean jouer;

	/**
  	* Le constructeur Joueur permet de définir les joueurs
	**/

	public Joueur() {}

	/**
	*	La méthode getPseudo() permet d'afficher le pseudo d'un joueur 
	*	@return Retourne le pseudo du joueur 
	**/

	public String getPseudo() {
		return this.pseudo;
	}

	/**
	*	La méthode setPseudo() permet de définir le pseudo d'un joueur
	*	@param pseudo permet de définir le pseudo du joueur
	**/

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	/**
	*	La méthode getCamp() permet d'afficher le camp d'un joueur 
	*	@return Retourne le camp du joueur 
	**/

	public String getCamp() {
		return this.camp;
	}

	/**
	*	La méthode setCamp() permet de définir le camp d'un joueur
	*	@param camp permet de définir le camp du joueur
	**/

	public void setCamp(String camp) {
		this.camp = camp;
	}

	/**
	*	La méthode getScore() permet d'afficher le score d'un joueur 
	*	@return Retourne le score du joueur 
	**/

	public int getScore() {
		return this.score;
	}

	/**
	*	La méthode setScore() permet de définir le score d'un joueur
	*	@param score permet de définir le score du joueur
	**/

	public void setScore(int score) {
		this.score = score;
	}

	/**
	*	La méthode getJouer() permet d'afficher si le joueur peut jouer
	*	@return Retourne vrai si le joueur peut jouer sinon il retourne faux
	**/

	public boolean getJouer() {
		return this.jouer;
	}

	/**
	*	La méthode setJouer() permet de définir si le joueur peut jouer
	*	@param jouer permet de définir si le joueur peut jouer
	**/

	public void setJouer(boolean jouer) {
		this.jouer = jouer;
	}

	/**
	*	La méthode toString() permet d'afficher un joueur
	*	@return Retourne si le joueur est en train de jouer ou non et retourne les paramètres du joueur
	**/

	public String toString()
	{
		String jouer;
		if(this.jouer) jouer = " ; en train de jouer";
		else jouer = " ; en attente de jouer";
		return "Joueur - "+this.pseudo+" ; camp : "+this.camp+" ; score : "+this.score+jouer;
	}

}
