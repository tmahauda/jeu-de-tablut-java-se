package jeu;
import clavier.*;

/**
* Cette classe est la classe Menu
*
* Ecrit par Kévin Migadel
* @author Kévin Migadel
* @version 1.0
**/

public class Menu {

	private Joueur[] joueurs;
	private boolean confirmerEnregistrement;
	private Clavier clavier;

	/**
  	* Le constructeur Menu permet d'initialiser la partie
	**/

	public Menu()
	{
		this.joueurs = new Joueur[2];
		this.joueurs[0] = new Joueur();
		this.joueurs[1] = new Joueur();
		this.clavier = new Clavier();
		this.initialiserPartie();
	}

	/**
	*	La méthode demanderRegleJeu() permet d'appeler la méthode afficherRegleJeu()
	*	@param i permet de savoir si le joueur veut lire les rêgles ou non. Si i = 1, le joueur veut lire les rêgles sinon il appuie sur 2.
	**/

	public void demanderRegleJeu(int i)
	{
		Interfaces interfaceRegle = new Interfaces();
		interfaceRegle.afficherRegleJeu(i);
	}

	/**
	*	La méthode lancerPartie() permet de prévenir les joueurs que le jeu va bientôt commencer.
	**/

	public void lancerPartie()
	{
		System.out.println("Le jeu va bientôt débuter. Veuillez patienter");
	}

	/**
	*	La méthode demanderPseudosJoueurs() permet au joueurs d'écrire leur pseudo et vérifie s'ils sont correct
	**/

	public void demanderPseudosJoueurs()
	{
		String pseudo1;
		String pseudo2;
		this.confirmerEnregistrement = false;
		System.out.println("Veuillez entrer les pseudos uniques des deux joueurs dans l'ordre respective supérieur à un caractère uniquement en alphabétique et/ou alphanumérique :");
		while(!this.confirmerEnregistrement)
		{
			try
			{
				pseudo1 = this.clavier.ecrirePseudo("Joueur 1 : ");
				pseudo2 = this.clavier.ecrirePseudo("Joueur 2 : ");
				Clavier.equalsPseudo(pseudo1,pseudo2);
				this.enregistrerPseudo(pseudo1,pseudo2);
				this.confirmerEnregistrement = true;
			}
			catch(ClavierException e)
			{
				System.out.println(e.getMessage());
			}
		}
	}

	/**
	*	La méthode demanderCampJoueurs() permet aux joueurs de choisir leur camp et leur demande de re-saisir s'ils se sont trompés
	**/

	public void demanderCampJoueurs()
	{
		String camp1;
		String camp2;
		this.confirmerEnregistrement = false;
		System.out.println("Veuillez choisir un camp "+this.joueurs[0].getPseudo());
		System.out.println("Pour défendre le camp des Moscovites, taper 1. Sinon pour défendre le camp des Suédois taper 2 :");
		while(!this.confirmerEnregistrement)
		{
			try
			{
				camp1 = this.clavier.ecrireCamp("Choix : ");
				camp2 = this.clavier.ecrireCamp();
				System.out.println(this.joueurs[0].getPseudo()+" a choisi le camp "+camp1);
				System.out.println("Par conséquent, "+this.joueurs[1].getPseudo()+" défendra le camp "+camp2);
				this.enregistrerCamp(camp1,camp2);
				this.confirmerEnregistrement = true;
			}
			catch(ClavierException e)
			{
				System.out.println(e.getMessage());
			}
		}
	}

	/**
	*	La méthode enregistrerPseudo() permet d'enregistrer le pseudo des deux joueurs
	*	@param pseudo1 représente le pseudo du joueur 1
	*	@param pseudo2 représente le pseudo du joueur 2
	**/

	public void enregistrerPseudo(String pseudo1, String pseudo2)
	{
		this.joueurs[0].setPseudo(pseudo1);
		this.joueurs[1].setPseudo(pseudo2);
	}

	/**
	*	La méthode enregistrerCamp() permet d'enregistrer le camp des deux joueurs
	*	@param camp1 représente le camp du joueur 1
	*	@param camp2 représente le camp du joueur 2
	**/

	public void enregistrerCamp(String camp1, String camp2)
	{
		this.joueurs[0].setCamp(camp1);
		this.joueurs[1].setCamp(camp2);
	}

	/**
	*	La méthode confirmerEnregistrement() permet de vérifier que les deux joueurs ont bien enregistrer leur choix
	*	@return Retourne vrai si l'enregistrement c'est bien passé et faux si l'enregistrement n'est pas terminé
	**/

	public boolean confirmerEnregistrement()
	{
		return this.confirmerEnregistrement;
	}

	/**
	*	La méthode initialiserPartie() permet de demander aux joueurs s'ils veulent consulter les rêgles du jeu avant de jouer et s'ils veulent lancer le jeu ou le quitter
	**/

	public void initialiserPartie()
	{
		Interfaces interfaceMenu = new Interfaces();
		interfaceMenu.afficherMenu();
		this.demanderPseudosJoueurs();
		this.demanderCampJoueurs();
		this.confirmerEnregistrement = false;
		System.out.println("Si vous souhaitez consulter les règles du jeu, taper 1. Sinon taper 2 pour lancer ou quitter le jeu");
		while(!this.confirmerEnregistrement)
		{
			try
			{
				if(this.clavier.demanderChoix())
				{
					this.demanderRegleJeu(1);
					if(this.clavier.demanderChoix()) this.demanderRegleJeu(2);
				}
				System.out.println("Si vous souhaitez confirmer le lancement du jeu, taper 1. Sinon taper 2 pour quitter");
				if(this.clavier.demanderChoix())
				{
					this.confirmerEnregistrement = true;
					this.lancerPartie();
				}
				else System.exit(1);
			}
			catch(ClavierException e)
			{
				System.out.println(e.getMessage());
			}
		}
	}

	/**
	*	La méthode getJoueur() permet d'afficher un joueur
	*	@param i permet de déterminer le joueur (1 = joueur1, 2 = joueur2)
	*	@return Retourne soit le joueur1, soit le joueur2
	**/

	public Joueur getJoueur(int i)
	{
		return this.joueurs[i];
	}

}
