package plateau;

/**
* Cette classe est la classe DeplacementException
*
* Ecrit par Kévin Migadel
* @author Kévin Migadel
* @version 1.0
**/

public class DeplacementException extends Exception {

	/**
  	* Le constructeur DeplacementException permet d'afficher une exception il fait appelle à sa super classe Exception
	* @param message permet d'afficher le message de l'exception
	**/

  	public DeplacementException(String message) {
    	super(message);
  	}
}
