package pion;
import plateau.Case;

/**
*
*	Cette classe est la classe Moscovites
*
*	Ecrit par Kévin Migadel
*	@author Kévin Migadel
*	@version 1.0
**/

public class Moscovites extends Pion {

	/**
	*	Le constructeur Moscovites est défini par la super classe Pion
	*	@param taille permet de définir la taille du pion
	*	@param couleur permet de connaître la couleur du pion
	*	@param camp permet de connaître le camps du pion
	*	@param cases permet de connaître la case où se situe le pion
	**/

	//Constructeur
	public Moscovites(int taille, String couleur, String camp, Case cases) {
		super(taille,couleur,camp,cases);
	}

	//Méthodes

	/**
	*	La méthode toString() permet d'afficher un moscovites 
	*	@return Retourne le type de pion et ses paramètres
	*
	**/

	public String toString() {
		return ("Pion : Moscovites" + super.toString());
	}

}
