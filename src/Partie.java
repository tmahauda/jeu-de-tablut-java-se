package jeu;
import plateau.*;
import jeu.*;
import pion.*;
import clavier.*;

/**
* Cette classe est la classe Partie
*
* Ecrit par Kévin Migadel
* @author Kévin Migadel
* @version 1.0
**/

public class Partie
{
	
	public static final String term_reset = "\u001B[0m";
	public static final String term_black = "\u001B[30m";
	public static final String term_red = "\u001B[31m";
	public static final String term_green = "\u001B[32m";
	public static final String term_yellow = "\u001B[33m";
	public static final String term_blue = "\u001B[34m";
	public static final String term_purple = "\u001B[35m";
	public static final String term_cyan = "\u001B[36m";
	public static final String term_white = "\u001B[37m";

	public static final String term_back_reset = "\u001B[40m";
	public static final String term_back_purple = "\u001B[45m";

	private int manche;
	private Menu menu;
	private Plateau plateau;
	private Clavier clavier;

	/**
  	* Le constructeur Partie permet de lancer le jeu
	**/

	public Partie() {this.lancerJeu();}

	/**
	*	La méthode initialiserManche() permet d'initialiser les manches 
	*	@param choix permet de savoir dans quelle manche sont les joueurs
	**/

	public void initialiserManche(int choix)
	{
		this.manche = choix;
		if(this.manche == 2)
		{
			String camp1, camp2;
			camp1 = this.menu.getJoueur(0).getCamp();
			camp2 = this.menu.getJoueur(1).getCamp();
			this.menu.getJoueur(0).setCamp(camp2);
			this.menu.getJoueur(1).setCamp(camp1);
		}
	}

	/**
	*	La méthode deplacerPion() permet de déplacer un pion sur une case 
	*	@param pion permet de choisir un pion
	*	@param cases permet de choisir une case
	*	@return Retourne vrai si le déplacement s'est bien passé et faux s'il le déplacement n'a pas été validé
	**/

	public boolean deplacerPion(Pion pion, Case cases)
	{
		int scorep1, scorep2;
		if(this.confirmerDeplacement(pion,cases))
		{
			System.out.println("Votre déplacement a bien été effectué");
			if(this.confirmerCapturer(pion))
			{
				System.out.println("Et vous avez capturé un pion adverse");
				//Mise a jour score
				Pion[] capture = this.plateau.getPions();
				if (this.manche == 2) {
					scorep1 = this.menu.getJoueur(0).getScore();
					scorep2 = this.menu.getJoueur(1).getScore();
				}
				else{
					scorep1 = 0;
					scorep2 = 0;
				}
				this.menu.getJoueur(0).setScore(scorep1);
				this.menu.getJoueur(1).setScore(scorep2);
				for (int i=0; i<25; i++) {
					if (capture[i].getCase() == null) {
						if (capture[i].getCamp() == "Moscovites") {
							if (this.menu.getJoueur(0).getCamp() == "Suedois") {
								this.menu.getJoueur(0).setScore(this.menu.getJoueur(0).getScore()+1);
							}
							else if (this.menu.getJoueur(1).getCamp() == "Suedois") {
								this.menu.getJoueur(1).setScore(this.menu.getJoueur(1).getScore()+1);
							}
						}
						else if (capture[i].getCamp() == "Suedois") {
							if (this.menu.getJoueur(0).getCamp() == "Moscovites") {
								this.menu.getJoueur(0).setScore(this.menu.getJoueur(0).getScore()+1);
							}
							else if (this.menu.getJoueur(1).getCamp() == "Moscovites") {
								this.menu.getJoueur(1).setScore(this.menu.getJoueur(1).getScore()+1);
							}
						}
					}
				}
			}
			else System.out.println("Mais vous n'avez pas capturé un pion adverse");
			return true;
		}
		else
		{
			System.out.println("Votre déplacement n'a pas été validé");
			return false;
		}
	}

	/**
	*	La méthode lancerJeu() permet de lancer la partie
	**/

	public void lancerJeu() {
		boolean gg = false;
		this.menu = new Menu();
		this.plateau = new Plateau();
		if(menu.confirmerEnregistrement())
		{
			this.initialiserManche(1);
			while(gg == false) {
				this.demanderJouer(0);
				if (plateau.getGg()) {
					gg = true;
				}
				else{
					this.demanderJouer(1);
				}
			}
			gg = false;
			this.confirmerGagner();
			this.initialiserManche(2);
			this.plateau = new Plateau();
			while(gg == false) {
				this.demanderJouer(1);
				if (plateau.getGg()) {
					gg = true;
				}
				else{
					this.demanderJouer(0);
				}
			}
			this.confirmerGagner();
			this.quitterJeu();
		}
	}

	/**
	*	La méthode quitterJeu() permet aux joueurs de quitter le jeu ou de rejouer
	**/

	public void quitterJeu()
	{
		Interfaces interfaceFinDePartie = new Interfaces();
		interfaceFinDePartie.afficherFinDePartie();
		System.out.println("Si vous souhaitez rejouer, taper 1. Sinon taper 2 pour quitter");
		try
		{
			if(this.clavier.demanderChoix()) new Partie();
			else System.exit(1);
		}
		catch(ClavierException e)
		{
			System.out.println(e.getMessage());
		}
	}

	/**
	*	La méthode demanderJouer() permet de demander aux joueurs de jouer
	*	@param i permet de dire à qui ont demande de jouer. (0 = joueur1, 1 = joueur2)
	**/

	public void demanderJouer(int i)
	{
		Interfaces interfacePartie = new Interfaces();
		interfacePartie.afficherPlateau(plateau);
		this.clavier = new Clavier();
		int x;
		int y;
		boolean clavierexpt = false;
		boolean depexpt = false;
		String error = "";
		Pion pion = null;
		Case cases = null;
		this.menu.getJoueur(i).setJouer(true);
		i = (i==0)? 1 : 0;
		this.menu.getJoueur(i).setJouer(false);
		i = (i==1)? 0 : 1;
		do
		{
			try
			{
				if (depexpt) {
					System.out.println(term_red+"*------------------------------------------------------*\n"+error);
					error = "";
					depexpt = false;
					System.out.println("*------------------------------------------------------*"+term_reset);
				}

				this.afficherInfos();
				System.out.println(this.menu.getJoueur(i).getPseudo()+" peut commencer à jouer");

				if (clavierexpt) {
					System.out.println(term_red+"*------------------------------------------------------*\n"+error);
					error = "";
					clavierexpt = false;
					System.out.println("*------------------------------------------------------*"+term_reset);
				}

				System.out.println("Veuillez choisir un pion "+this.menu.getJoueur(i).getCamp()+" par ses coordonnées x et y ");
				x = this.clavier.ecrireCoord("x : ");
				y = this.clavier.ecrireCoord("y : ");
				pion = this.plateau.getPion(this.plateau.getCase(x,y),this.menu.getJoueur(i));// Méthode getPion lance exception DeplacementException si essaye de récupérer un camp adverse
				System.out.println("Choissisez maintenant les coordonnées x et y de la case pour déplacer votre pion");
				x = this.clavier.ecrireCoord("x : ");
				y = this.clavier.ecrireCoord("y : ");
				cases = this.plateau.getCase(x,y);
			}
			catch(ClavierException e)
			{
				clavierexpt = true;
				error = e.getMessage();
			}
			catch(DeplacementException e)
			{
				depexpt = true;
				error = e.getMessage();
			}
			interfacePartie.afficherPlateau(plateau);
		}while(!this.deplacerPion(pion, cases));
	}

	/**
	*	La méthode confirmerCapturer() permet de savoir si un pion à été capturé
	*	@param pion représente un Pion
	*	@return Retourne vrai si un pion s'est fait capturé ou faux s'il ne s'est pas fait capturé
	**/

	public boolean confirmerCapturer(Pion pion)
	{
		return this.plateau.verifierCapturer(pion);
	}

	/**
	*	La méthode confirmerDeplacement() permet de savoir si un pion s'est corectement déplacé sur une case
	*	@param pion représente un pion
	*	@param cases représente une case
	*	@return Retourne vrai si un pion s'est correctement déplacer sur case ou faux s'il ne s'est pas corectement déplacé sur une case
	**/

	public boolean confirmerDeplacement(Pion pion, Case cases)
	{
		try{
		return this.plateau.verifierDeplacement(pion,cases);
		}
		catch(DeplacementException e){
			System.out.println(term_red+"*------------------------------------------------------*\n"+e.getMessage()+"\n*------------------------------------------------------*" +term_reset);
			return false;
		}
	}

	/**
	*	La méthode confirmerGagner() permet de savoir si un des deux joueurs à gagnés
	**/

	public void confirmerGagner()
	{

		Interfaces interfaceGagner = new Interfaces();
		this.clavier = new Clavier();
		int y;
		String affiche;
		try
		{

			if(this.manche == 1)
			{
				if(this.plateau.getGgmosco()) interfaceGagner.afficherGagner(1);
				else interfaceGagner.afficherGagner(2);
				affiche = "Qui ont gagnés la première manche avec un score total de ";

					if (this.plateau.getGgmosco() == true && this.menu.getJoueur(0).getCamp() == "Moscovites") {
						affiche += ""+this.menu.getJoueur(0).getScore();
					}
					else if (this.plateau.getGgmosco() == true && this.menu.getJoueur(1).getCamp() == "Moscovites") {
						affiche += ""+this.menu.getJoueur(1).getScore();
					}
					else if (this.plateau.getGgsuedois() == true && this.menu.getJoueur(0).getCamp() == "Suedois") {
						affiche += ""+this.menu.getJoueur(0).getScore();
					}
					else if (this.plateau.getGgsuedois() == true && this.menu.getJoueur(1).getCamp() == "Suedois") {
						affiche += ""+this.menu.getJoueur(1).getScore();
					}
					System.out.println(affiche);

				y = this.clavier.ecrireCoord("");
			}
			if(this.manche == 2)
			{
				if(this.plateau.getGgmosco()) interfaceGagner.afficherGagner(1);
				else interfaceGagner.afficherGagner(2);
						affiche = "Qui ont gagnés la deuxième manche avec un score total de ";

					if (this.plateau.getGgmosco() == true && this.menu.getJoueur(0).getCamp() == "Moscovites") {
						affiche += ""+this.menu.getJoueur(0).getScore();
					}
					else if (this.plateau.getGgmosco() == true && this.menu.getJoueur(1).getCamp() == "Moscovites") {
						affiche += ""+this.menu.getJoueur(1).getScore();
					}
					else if (this.plateau.getGgsuedois() == true && this.menu.getJoueur(0).getCamp() == "Suedois") {
						affiche += ""+this.menu.getJoueur(0).getScore();
					}
					else if (this.plateau.getGgsuedois() == true && this.menu.getJoueur(1).getCamp() == "Suedois") {
						affiche += ""+this.menu.getJoueur(1).getScore();
					}
					System.out.println(affiche);

				y = this.clavier.ecrireCoord("");
			}
		}
		catch(ClavierException e)
			{
				System.out.println(e.getMessage());
			}
	}

	/**
	*	La méthode afficherInfos() permet d'afficher les informations de chaque joueurs
	**/

	public void afficherInfos()
	{
		System.out.println("Nous sommes actuellement en manche "+this.manche+"\n");
		System.out.println("Etats actuels des joueurs : ");
		for(int i=0;i<2;i++)
		{
			System.out.println(this.menu.getJoueur(i).toString());
		}
		System.out.print("\n");
	}

	/**
	*	La méthode demanderAide() permet d'afficher une aide si le joueur le désire
	**/

	public void demanderAide()
	{
		//TODO
	}

}
