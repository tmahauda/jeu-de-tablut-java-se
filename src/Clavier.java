package clavier;
import java.util.Scanner;
import jeu.Interface;

/**
* Cette classe est la classe Clavier
*
* Ecrit par Kévin Migadel
* @author Kévin Migadel
* @version 1.0
**/

public class Clavier
{
	private Scanner sc;
	private String entree;

	/**
  	* Le constructeur Clavier permet aux l'utilisateurs d'intéragir avec le jeu
	**/

	public Clavier() {
		this.sc = new Scanner(System.in);
		this.entree = new String();
	}

	/**
	*	La méthode ecrirePseudo() permet de vérifier si les pseudos des joueurs est correct
	*	@param message représente le pseudo du joueur et permet de vérifier s'il est correct
	*	@throws ClavierException si le pseudo ne respecte pas la syntaxe 
	*	@return Le pseudo du joueur
	**/

	public String ecrirePseudo(String message) throws ClavierException {
		System.out.print(message);
		this.entree = this.sc.nextLine();
		if(this.entree.matches("[a-zA-Z0-9áàâäéèêëíìîïóòôöúùûü]{2,}"))
		return this.entree;
		else throw new ClavierException("Le pseudo du joueur ne respecte pas la syntaxe. Re-saisir");
	}

	/**
	*	La méthode ecrireCamp() permet de vérifier si le joueur à correctement choisit son camp 
	*	@param message permet de vérifier si le joueur à correctement choisit son camp
	*	@throws ClavierException si le joueur écrit quelques choses d'autres que 1 ou 2 
	*	@return Moscovites si le joueur à saisit 1 et Suédois si le joueur à saisit 2
	**/

	public String ecrireCamp(String message) throws ClavierException {
		System.out.print(message);
		this.entree = this.sc.nextLine();
		if(this.entree.equals("1")) return "Moscovites";
		else if (this.entree.equals("2")) return "Suedois";
		else throw new ClavierException();
	}

	/**
	*	La méthode ecrireCamp() permet de définir à l'autre joueur le second camp
	*	@return Moscovites ou Suédois en fonction de ce qu'à choisit le premier joueur 
	**/

	public String ecrireCamp() {
		if(this.entree.equals("1")) return "Suedois";
		else return "Moscovites";
	}

	/**
	*	La méthode demanderChoix() permet à l'utilisateur de choisir si oui ou non il veut accepter la requète proposer par le jeu
	*	@throws ClavierException si le joueur écrit quelques choses d'autres que 1 ou 2 
	*	@return Vrai si le joueur saisit 1 et faux si le joueur saisit 2 
	**/

	public boolean demanderChoix() throws ClavierException {
		System.out.print("Choix : ");
		this.entree = this.sc.nextLine();
		if(this.entree.equals("1")) return true;
		else if(this.entree.equals("2")) return false;
		else throw new ClavierException();
	}

	/**
	*	La méthode equalsPseudo() permet de vérifier si deux pseudos sont identiques 
	*	@param pseudo1 représente le pseudo du premier joueur
	*	@param pseudo2 représente le pseudo du second joueur
	*	@throws ClavierException si le pseudo des deux joueurs sont identiques 
	**/

	public static void equalsPseudo(String pseudo1, String pseudo2) throws ClavierException {
		if(pseudo1.equals(pseudo2))
		{
			throw new ClavierException("Le nom des joueurs sont égaux. Re-saisir");
		}
	}

	/**
	*	La méthode ecrireCoord() permet aux joueurs de choisir les coordonnées d'une case du plateau 
	*	@param message permet aux joueurs de choisir les coordonnées d'une case du plateau
	*	@throws ClavierException si les coordonnées saisit par le joueur ne se situe pas sur le plateau
	*	@return L'entier saisit par le joueur
	**/

	public int ecrireCoord(String message) throws ClavierException {
		System.out.print(message);
		this.entree = this.sc.nextLine();
		if(this.entree.matches("[0-8]")) return Integer.valueOf(this.entree);
		else throw new ClavierException("Les coordonnées de la case n'existe pas, choisir une case hors du plateau n'est pas une bonne idée ! Re-saisir");
	}

}
