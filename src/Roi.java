package pion;
import plateau.*;

/**
*	Cette classe est la classe Roi  
*
*	Ecrit par Kévin Migadel
*	@author Kévin Migadel
*	@version 1.0
**/

public class Roi extends Pion {

	/**
	*	Le constructeur Roi est défini par la super classe Pion
	*	@param taille permet de définir la taille du pion
	*	@param couleur permet de connaître la couleur du pion
	*	@param camp permet de connaître le camps du pion
	*	@param cases permet de connaître la case où se situe le pion
	**/

	public Roi(int taille, String couleur, String camp, Case cases){
		super(taille,couleur,camp,cases);
	}

	//Méthodes

	/**
	*	La méthode toString() permet d'afficher le Roi 
	*	@return Retourne le type de pion et ses paramètres
	**/

	public String toString() {
		return ("Pion : Roi" + super.toString());
	}

	/**
	*	La méthode verifierDeplacementPion() permet de vérifier le déplacement du Roi qui sont différents des autres pions grâce à la classe DeplacementException
	*	@param x1 est l'abscisse initiale du pion
	*	@param y1 est l'ordonnée initiale du pion
	*	@param x2 est l'abscisse finale du pion
	*	@param y2 est l'ordonnée finale du pion
	*	@return Retourne une exception si les coordonnées initiales et finales sont les mêmes ou si elles sont éronnées.
	*	@throws DeplacementException si les nouvelles et anciennes coordonnées sont égales
	*	@throws DeplacementException si on déplace le Roi de plus de 4 cases horizontalement
	*	@throws DeplacementException si on déplace le Roi de plus de 4 cases verticalement 
	*	@throws DeplacementException si on déplace le Roi diagonalement
	*	@throws DeplacementException si une erreur innatendue survient
	**/

	public boolean verifierDeplacementPion(int x1, int y1, int x2, int y2) throws DeplacementException {

		if (x1 == x2 && y1 == y2) {
			throw new DeplacementException ("Si les nouvelles et anciennes coordonnées sont égales, le pion ne se déplace pas !");
		}
		else{
			if (x1 == x2 && Math.abs(y1-y2) > 4) {
				throw new DeplacementException ("Le Roi ne peut se déplacer que de 4 cases maximum.");
			}
			else if (y1 == y2 && Math.abs(x1-x2) > 4){
				throw new DeplacementException ("Le Roi ne peut se déplacer que de 4 cases maximum.");
			}
			else if(x1 != x2 && y1 != y2){
				throw new DeplacementException ("Le Roi ne peut se déplacer que horizontalement ou verticalement");
			}
			else if ((x1 == x2 && Math.abs(y1-y2) <= 4) || (y1 == y2 && Math.abs(x1-x2) <= 4)){
				return true;
			}
			else{
				throw new DeplacementException ("Erreur innatendue, veuillez contatacter le support");
			}
		}
	}

}
