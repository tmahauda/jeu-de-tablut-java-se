package pion;
import plateau.*;

/**
*	Cette classe est la classe abstraite Pion qui définit par héritage les classes Roi, Soldat et Moscovites 
*
*	Ecrit par Kévin Migadel
*	@author Kévin Migadel
*	@version 1.0
**/

public abstract class Pion {
    protected int taille;
	protected String couleur;
    protected String camp;
    protected Case cases;

	/**
	*	Le constructeur Pion permet de définir les classes Roi, Soldat et Moscovites
	*	@param taille permet de définir la taille du pion
	*	@param couleur permet de connaître la couleur du pion
	*	@param camp permet de connaître le camps du pion
	*	@param cases permet de connaître la case où se situe le pion
	**/

	//Constructeur
    public Pion(int taille, String couleur, String camp, Case cases){
        this.taille = taille;
        this.couleur = couleur;
        this.camp = camp;
        this.cases = cases;
    }

    //Méthodes

	/**
	*	La méthode toString() permet d'afficher un pion 
	*	@return Retourne les paramètres d'un pion 
	**/

    public String toString() {
        return (" ; Taille : " + this.taille + " ; Couleur : " + this.couleur + " ; Camp : " + this.camp +" ; Case : " + this.cases.toString());
    }

	/*
    public void attribuerCouleurTaille(String couleur, int taille) {
        //TODO
    }
	*/

	/**
	*	La méthode getCase() permet d'afficher la case d'un pion 
	*	@return Retourne une case
	**/

    public Case getCase() {
      	return this.cases;
    }

    /**
	*	La méthode setCase() permet de définir une case pour un pion
	*	@param cases permet de définir une case à un pion
	**/

    public void setCase(Case cases) {
        this.cases = cases;
    }

    /**
	*	La méthode getCouleur() permet de d'afficher la couleur d'un pion
	*	@return Retourne la couleur du pion
	**/

    public String getCouleur() {
        return this.couleur;
    }

    /**
	*	La méthode setCouleur() permet de définir la couleur d'un pion
	*	@param couleur permet de définir la couleur d'un pion
	**/

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    /**
	*	La méthode getTaille() permet de d'afficher la taille d'un pion
	*	@return Retourne la taille du pion
	**/

    public int getTaille() {
        return this.taille;
    }

    /**
	*	La méthode setTaille() permet de définir la taille d'un pion
	*	@param taille permet de définir la taille d'un pion
	**/

    public void setTaille(int taille) {
        this.taille = taille;
    }

    /**
	*	La méthode getCamp() permet de d'afficher le nom du camps d'un pion
	*	@return Retourne le nom du camps d'un pion
	**/

    public String getCamp() {
        return this.camp;
    }

    /**
	*	La méthode setCamp() permet de définir le nom du camps d'un pion
	*	@param camp permet de définir le nom du camps d'un pion
	**/

    public void setCamp(String camp) {
        this.camp = camp;
    }

    /**
	*	La méthode verifierDeplacementPion() permet de vérifier le déplacement d'un pion grâce à la classe DeplacementException
	*	@param x1 est l'abscisse initiale du pion
	*	@param y1 est l'ordonnée initiale du pion
	*	@param x2 est l'abscisse finale du pion
	*	@param y2 est l'ordonnée finale du pion
	*	@return Retourne une exception si les coordonnées initiales et finales sont les mêmes ou si elles sont éronnées.
	*	@throws DeplacementException si les nouvelles et anciennes coordonnées sont égales
	*	@throws DeplacementException si on déplace un pion diagonalement 
	*	@throws DeplacementException si on sort un pion du plateau
	*	@throws DeplacementException si une erreur innatendue survient
	**/

	public boolean verifierDeplacementPion(int x1, int y1, int x2, int y2) throws DeplacementException {

		if (x1 == x2 && y1 == y2) {
			throw new DeplacementException ("Si les nouvelles et anciennes coordonnées sont égales, le pion ne se déplace pas !");
		}
		else{
			if(x1 != x2 && y1 != y2){
				throw new DeplacementException ("Les pions ne peuvent se déplacer que horizontalement ou verticalement (il ne peut pas se déplacer autrement)");
			}
			else if (x2 >= 9 || x2 <0 || y2 >= 9 || y1 <0){
				throw new DeplacementException ("Sortir un pion du plateau n'est pas recomandé");
			}
			else if ((x1 == x2 || y1 == y2)){
				return true;
			}
			else{
				throw new DeplacementException ("Erreur innatendue, veuillez contatacter le support");
			}
		}
	}

}
