package plateau;
import pion.*;

/**
* Cette classe est la classe Case
*
* Ecrit par Kévin Migadel
* @author Kévin Migadel
* @version 1.0
**/

public class Case {

  	private int x, y;
  	private int couleur; // 0=Blanche ; 1=Beige ; 2=Rose ; 3=Noir (trône)
  	private int etat; // 0=vide ; 1=suédois ; 2=Moscovites ; 3=Roi

	/**
  	* Le constructeur Case permet de définir les différentes cases du plateau
	* @param x permet de connaître l'abscisse d'une case
	* @param y permet de connaître l'ordonnée d'une case
	**/

	public Case(int x, int y) {
		this.x = x;
		this.y = y;
		this.etat = 0;
		this.attribuerCouleur();
	}

  	//Get et set

	/**
	*	La méthode getX() permet d'afficher l'abscisse de la case 
	*	@return Retourne l'abscisse de la case
	**/

  	public int getX() {
		return this.x;
  	}

  	/**
	*	La méthode getY() permet d'afficher l'ordonnée de la case 
	*	@return Retourne l'ordonnée de la case
	**/

	public int getY() {
		return this.y;
	}

	/**
	*	La méthode getCouleur() permet d'afficher la couleur de la case 
	*	@return Retourne la couleur de la case
	**/

	public int getCouleur() {
		return this.couleur;
	}

	/**
	*	La méthode getEtat() permet d'afficher l'état de la case 
	*	@return Retourne l'état de la case
	**/

	public int getEtat() {
		return this.etat;
	}

	/**
	*	La méthode setEtat() permet de définir l'état d'une case
	*	@param i permet de définir l'état de la case. Voici les différents états 0 = vide, 1 = suédois, 2 = Moscovites, 3 = Roi
	**/

	public void setEtat(int i) {
		this.etat = i;
	}

	/**
	*	La méthode toString() permet d'afficher une case
	*	@return Retourne l'abscisse et l'ordonnée d'une case
	**/

	public String toString() {
		return "x = "+this.x+" et y = "+this.y;
	}

	//Methodes

	/**
	*	La méthode attribuerCouleur() permet d'attribuer une couleur à une case.
	*	Voici les différentes couleurs possibles : 0 = Blanche, 1 = Beige, 2 = Rose, 3 = Noir (trône)
	**/

	public void attribuerCouleur() {
		switch (this.x) {
			case 0:
			  	switch (this.y){
					case 3:
				  		this.couleur=2;
					break;
					case 4:
				  		this.couleur=2;
					break;
					case 5:
				  		this.couleur=2;
					break;
			  	}
				break;
			case 1:
			  	switch (this.y){
					case 4:
				  		this.couleur=2;
			  	}
			break;
			case 2:
			  	switch (this.y){
					case 4:
				  		this.couleur=1;
			  	}
			break;
			case 3:
			  	switch (this.y){
					case 0:
				  		this.couleur=2;
					break;
					case 4:
				  		this.couleur=1;
					break;
					case 8:
				  		this.couleur=2;
					break;
			  	}
			break;
			case 4:
			  	switch (this.y){
					case 0:
				  		this.couleur=2;
					break;
					case 1:
				  		this.couleur=2;
					break;
					case 2:
					  	this.couleur=1;
					break;
					case 3:
					  	this.couleur=1;
					break;
					case 4:
					  	this.couleur=3;
					break;
					case 5:
					  	this.couleur=1;
					break;
					case 6:
					  	this.couleur=1;
					break;
					case 7:
					  	this.couleur=2;
					break;
					case 8:
					  	this.couleur=2;
					break;
				 }
			break;
			case 5:
				switch (this.y){
					case 0:
					  	this.couleur=2;
					break;
					case 4:
					  	this.couleur=1;
					break;
					case 8:
					  	this.couleur=2;
					break;
				}
			break;
			case 6:
			  	switch (this.y){
					case 4:
				  		this.couleur=1;
			  	}
			break;
			case 7:
			  	switch (this.y){
					case 4:
				  		this.couleur=2;
			  	}
			break;
			case 8:
			  	switch (this.y){
					case 3:
				  		this.couleur=2;
					break;
					case 4:
				  		this.couleur=2;
					break;
					case 5:
				  		this.couleur=2;
					break;
			  	}
			break;
		}
	}

}
