package jeu;
import plateau.*;

/**
* Cette classe est la classe Interfaces
*
* Ecrit par Kévin Migadel
* @author Kévin Migadel
* @version 1.0
**/

public class Interfaces {

	/**
  	* Le constructeur Interfaces permet d'afficher le plateau et les rêgles du jeu
	**/

	public Interfaces(){}

	/**
	*	La méthode afficherPlateau() permet aux joueurs d'écrire leur pseudo
	*	@param plateau permet d'initialiser le plateau
	**/

	public void afficherPlateau(Plateau plateau) {
		clearScreen();
		System.out.println("Plateau :" + "\n");
		String retour = "    ";
		for(int i = 0; i < 9; i++){
			retour += "  " + i + " ";
		}
		retour += "\n";
		for(int j = 0; j < 9; j++){
			retour += "    ";
			for(int i = 0; i < 9; i++){
				retour += "+---";
			}
			retour += "+\n  " + j + " ";
			for(int i = 0; i < 9; i++){
				if (plateau.getCase(i,j).getEtat() == 0) {
					retour += "|" ;
					if(plateau.getCase(i,j).getCouleur() == 2){
						retour += Partie.term_back_purple+"   "+Partie.term_back_reset;
					}
					else{
						retour += "   ";
					}
				}
				else if(plateau.getCase(i,j).getEtat() == 1) {
					retour += "|";
					if(plateau.getCase(i,j).getCouleur() == 2){
						retour += Partie.term_back_purple+" s "+Partie.term_back_reset;
					}
					else{
						retour += " s ";
					}
				}
				else if(plateau.getCase(i,j).getEtat() == 2) {
					retour += "|";
					if(plateau.getCase(i,j).getCouleur() == 2){
						retour += Partie.term_back_purple+" m "+Partie.term_back_reset;
					}
					else{
						retour += " m ";
					}
				}
				else if(plateau.getCase(i,j).getEtat() == 3) {
					retour += "|";
					if(plateau.getCase(i,j).getCouleur() == 2){
						retour += Partie.term_back_purple+" R "+Partie.term_back_reset;
					}
					else{
						retour += " R ";
					}
				}
			}
			retour += "|\n";
		}
		retour += "    ";
		for(int i = 0; i < 9; i++){
			retour += "+---";
		}
		retour += "+\n";
		System.out.println(retour);
		System.out.println("Symboles : m pour Moscovites, s pour soldat des Suédois et R pour Roi des Suédois\n");
		System.out.println("N'oubliez pas que la case centrale du tableau  représente le trône et qu'il peut vous aider à la capture du Roi !");
	}

	/**
	*	La méthode afficherFinDePartie() permet de signaler aux joueurs la fin de la partie
	**/

	public void afficherFinDePartie()
	{
		clearScreen();
		System.out.println("\n\n\t\t\t\t\t\t* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *");
		System.out.println("\t\t\t\t\t\t*  _____ ___ _   _    ____  _____   ____   ___  ____ _____ ___ _____  *");
		System.out.println("\t\t\t\t\t\t* |  ___|_ _| \\ | |  |  _ \\| ____| |  _ \\ / _ \\|  _ \\_   _|_ _| ____| *");
		System.out.println("\t\t\t\t\t\t* | |_   | ||  \\| |  | | | |  _|   | |_) | |_| | |_) || |  | ||  _|   *");
		System.out.println("\t\t\t\t\t\t* |  _|  | || |\\  |  | |_| | |___  |  __/|  _  |  _ < | |  | || |___  *");
		System.out.println("\t\t\t\t\t\t* |_|   |___|_| \\_|  |____/|_____| |_|   |_| |_|_| \\_\\|_| |___|_____| *");
		System.out.println("\t\t\t\t\t\t* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\n\n");
	}

	/**
	*	La méthode afficherMenu() permet d'afficher le menu du jeu aux joueurs
	**/

	public void afficherMenu()
	{
		clearScreen();
		System.out.println("\n\n\t\t\t\t\t* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *");
		System.out.println("\t\t\t\t\t*  ____ ___ _____ _   ___     _______ _   _ _   _ _____   ___    __   __  _ ____     _    _____   *");
		System.out.println("\t\t\t\t\t* | __ )_ _| ____| \\ | \\ \\   / / ____| \\ | | | | | ____| |  _ \\ / _ \\| \\ | / ___|   | |  | ____|  *");
		System.out.println("\t\t\t\t\t* |  _ \\| ||  _| |  \\| |\\ \\ / /|  _| |  \\| | | | |  _|   | | | | |_| |  \\| \\___ \\   | |  |  _|    *");
		System.out.println("\t\t\t\t\t* | |_) | || |___| |\\  | \\ v / | |___| |\\  | |_| | |___  | |_| |  _  | |\\  |___) |  | |__| |___   *");
		System.out.println("\t\t\t\t\t* |____/___|_____|_| \\_|  \\_/  |_____|_| \\_|\\___/\\_____| |____/|_| |_|_| \\_|____/   |____|_____|  *");
		System.out.println("\t\t\t\t\t*                                                                                                 *");
		System.out.println("\t\t\t\t\t*\t      _ _____ _   _    ____  _   _    _____ ___  ____  _    _   _ _____                   *");
		System.out.println("\t\t\t\t\t*\t     | | ____| | | |  |  _ \\| | | |  |_   _/ _ \\| __ )| |  | | | |_   _|                  *");
		System.out.println("\t\t\t\t\t*\t  _  | |  _| | | | |  | | | | | | |    | || |_| |  _ \\| |  | | | | | |                    *");
		System.out.println("\t\t\t\t\t*\t | |_| | |___| |_| |  | |_| | |_| |    | ||  _  | |_) | |___ |_| | | |                    *");
		System.out.println("\t\t\t\t\t*\t  \\___/|_____|\\___/   |____/ \\___/     |_||_| |_|____/|_____\\___/  |_|                    *");
		System.out.println("\t\t\t\t\t* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\n\n");
	}

	/**
	*	La méthode afficherRegleJeu() permet d'afficher les rêgles du jeu aux joueurs
	*	@param partie permet d'afficher les rêgles du jeu en fonction du choix de l'utilisateur
	**/

	public void afficherRegleJeu(int partie)
	{
		if(partie == 1)
		{
			clearScreen();
			System.out.println("\n\n\t\t\t\t\t\t* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *");
			System.out.println("\t\t\t\t\t\t*  ____  _____ ____ _     _____ ____    ____  _   _       _ _____ _   _   *");
			System.out.println("\t\t\t\t\t\t* |  _ \\| ____/ ___| |   | ____/ ___|  |  _ \\| | | |     | | ____| | | |  *");
			System.out.println("\t\t\t\t\t\t* | |_) |  _|| |  _| |   |  _| \\___ \\  | | | | | | |  _  | |  _| | | | |  *");
			System.out.println("\t\t\t\t\t\t* |  _ <| |___ |_| | |___| |___ ___) | | |_| | |_| | | |_| | |___| |_| |  *");
			System.out.println("\t\t\t\t\t\t* |_| \\_\\_____\\____|_____|_____|____/  |____/ \\___/   \\___/|_____|\\___/   *");
			System.out.println("\t\t\t\t\t\t* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\n\n");
			System.out.println("La partie se déroule en 2 manches pour que chaque joueur puissent jouer les deux camps." + "\n");
			System.out.println("Les conditions de victoires sont les suivantes :");
			System.out.println("	-Le joueur ayant gagner les 2 parties est élu vainqueur.");
			System.out.println("	-En cas d'égalité, le joueur ayant capturé le plus de pions pendant les deux parties gagne." + "\n");

			System.out.println("Chaque camps à sa particularité, commençons par les moscovites :" + "\n");
			System.out.println("Le but premier des moscovites est de capturer le Roi qui se trouve au centre du plateau.");
			System.out.println("Pour le capturer, il suffit que le Roi soit entouré de Moscovites, c.a.d les cases situées en haut, en bas, à droite et à gauche de sa ");
			System.out.println("position. Par exemple :" + "\n");

			System.out.println("\t\t\t\t\t+-----+-----+-----+\t\t\t\t\tLégenge : ");
			System.out.println("\t\t\t\t\t|     |  m  |     |\t\t\t\t\t- 'm' représentent les moscovites.");
			System.out.println("\t\t\t\t\t+-----+-----+-----+\t\t\t\t\t- 'R' représentent le Roi.");
			System.out.println("\t\t\t\t\t|  m  |  R  |  m  |\t\t\t\t\t");
			System.out.println("\t\t\t\t\t+-----+-----+-----+\t\t\t\t\t");
			System.out.println("\t\t\t\t\t|     |  m  |     |\t\t\t\t\t");
			System.out.println("\t\t\t\t\t+-----+-----+-----+\t\t\t\t\t");
			System.out.println("\n");

			System.out.println("Mais il est possible de capturer le Roi avec seulement 3 moscovites ! En effet, la case située au centre du plateau nommée 'Trone'n'est accessible que par le Roi. ");
			System.out.println("En conséquence, si celui-ci est situé sur une case adjacente à cette case alors seulement 3 moscovites sontnecessaire. Voici un exemple :" + "\n");

			System.out.println("\t\t\t\t\t+-----+-----+-----+\t\t\t\t\tLégenge : ");
			System.out.println("\t\t\t\t\t|     |  m  |     |\t\t\t\t\t- 'm' représentent les moscovites.");
			System.out.println("\t\t\t\t\t+-----+-----+-----+\t\t\t\t\t- 'R' représentent le Roi.");
			System.out.println("\t\t\t\t\t|  *  |  R  |  m  |\t\t\t\t\t- '*' représentent le trône.");
			System.out.println("\t\t\t\t\t+-----+-----+-----+\t\t\t\t\t");
			System.out.println("\t\t\t\t\t|     |  m  |     |\t\t\t\t\t");
			System.out.println("\t\t\t\t\t+-----+-----+-----+\t\t\t\t\t");
			System.out.println("\n");

			System.out.println("Les moscovites peuvent également capturer des soldats Suédois. Pour les capturer, il faut au moins deux moscovites, il doivent se situer sur les côtés opposés ");
			System.out.println("de la case sur laquelle se situe le soldat Suédois. Par exemple :" + "\n");

			System.out.println("\t\t\t\t\t+-----+-----+-----+\t\t\t\t\tLégenge : ");
			System.out.println("\t\t\t\t\t|  m  |  s  |  m  |\t\t\t\t\t- 'm' représentent les moscovites.");
			System.out.println("\t\t\t\t\t+-----+-----+-----+\t\t\t\t\t- 's' représentent les soldats.");
			System.out.println("\n");

			System.out.println("Taper 1 pour continuer la lecture. Sinon taper 2 pour lancer le jeu");
		}

		else if (partie == 2)
		{
			clearScreen();
			System.out.println("Il est possible de capturer plusieurs en même temps par exemple :");

			System.out.println("Ici, les soldats capture deux moscovites." + "\n");
			System.out.println("\t\t\t\t\t+-----+-----+-----+\t\t\t\t\tLégenge : ");
			System.out.println("\t\t\t\t\t|  m  |  s  |  m  |\t\t\t\t\t- 'm' représentent les moscovites.");
			System.out.println("\t\t\t\t\t+-----+-----+-----+\t\t\t\t\t- 'R' représentent le Roi.");
			System.out.println("\t\t\t\t\t|  s  |     |     |\t\t\t\t\t");
			System.out.println("\t\t\t\t\t+-----+-----+-----+\t\t\t\t\t");
			System.out.println("\t\t\t\t\t|  m  |     |     |\t\t\t\t\t");
			System.out.println("\t\t\t\t\t+-----+-----+-----+\t\t\t\t\t");
			System.out.println("\n");

			System.out.println("Maintenant, voyons les soldats du Roi :" + "\n");
			System.out.println("Leur but est de protéger le Roi des Moscovites par tout les moyens possibles. Ainsi, ils peuvent capturer les Moscovites." + "\n");
			System.out.println("Les soldat Suédois on les mêmes propriétés que les moscovites. Ainsi, ils peuvent capturer de la même manière, par exemple :" + "\n");

			System.out.println("\t\t\t\t\t+-----+-----+-----+\t\t\t\t\tLégenge : ");
			System.out.println("\t\t\t\t\t|  s  |  m  |  s  |\t\t\t\t\t- 'm' représentent les moscovites.");
			System.out.println("\t\t\t\t\t+-----+-----+-----+\t\t\t\t\t- 's' représentent les soldats.");
			System.out.println("\n");

			System.out.println("Il est possible de capturer plusieurs en même temps par exemple :");

			System.out.println("Ici, les soldats capture deux moscovites." + "\n");
			System.out.println("\t\t\t\t\t+-----+-----+-----+\t\t\t\t\tLégenge : ");
			System.out.println("\t\t\t\t\t|  s  |  m  |  s  |\t\t\t\t\t- 'm' représentent les moscovites.");
			System.out.println("\t\t\t\t\t+-----+-----+-----+\t\t\t\t\t- 'R' représentent le Roi.");
			System.out.println("\t\t\t\t\t|  m  |     |     |\t\t\t\t\t");
			System.out.println("\t\t\t\t\t+-----+-----+-----+\t\t\t\t\t");
			System.out.println("\t\t\t\t\t|  s  |     |     |\t\t\t\t\t");
			System.out.println("\t\t\t\t\t+-----+-----+-----+\t\t\t\t\t");
			System.out.println("\n");

			System.out.println("Maintenant, voyons le Roi :" + "\n");
			System.out.println("Le but du Roi est d'atteindre l'extremité du plateau sans être capturé par les moscovites. Il ne peut se déplacer que de 4 cases contrairement aux autres pionts. ");
			System.out.println("S'il n'a qu'une possibilité, alors le jeu annonce 'Raichi', s'il y a deux ou plusieurs possibilités le jeu annonce 'Tuichi'. Par exemple : " + "\n");

			System.out.println("Exemple de Raichi :" + "\n");
			System.out.println("\t\t\t\t\t+-----+-----+-----+\t\t\t\t\tLégenge : ");
			System.out.println("\t\t\t\t\t|  s  |     |  s  |\t\t\t\t\t- 'm' représentent les moscovites.");
			System.out.println("\t\t\t\t\t+-----+-----+-----+\t\t\t\t\t- 'R' représentent le Roi.");
			System.out.println("\t\t\t\t\t|  m  |     |     |\t\t\t\t\t");
			System.out.println("\t\t\t\t\t+-----+-----+-----+\t\t\t\t\t");
			System.out.println("\t\t\t\t\t|  m  |  R  |     |\t\t\t\t\t");
			System.out.println("\t\t\t\t\t+-----+-----+-----+\t\t\t\t\t");
			System.out.println("\n");

			System.out.println("Exemple de Tuichi :" + "\n");
			System.out.println("\t\t\t\t\t+-----+-----+-----+\t\t\t\t\tLégenge : ");
			System.out.println("\t\t\t\t\t|  s  |     |  s  |\t\t\t\t\t- 'm' représentent les moscovites.");
			System.out.println("\t\t\t\t\t+-----+-----+-----+\t\t\t\t\t- 'R' représentent le Roi.");
			System.out.println("\t\t\t\t\t|  m  |     |     |\t\t\t\t\t");
			System.out.println("\t\t\t\t\t+-----+-----+-----+\t\t\t\t\t");
			System.out.println("\t\t\t\t\t|     |  R  |     |\t\t\t\t\t");
			System.out.println("\t\t\t\t\t+-----+-----+-----+\t\t\t\t\t");
			System.out.println("\n");

			System.out.println("Nota Bene : Les captures fonctionnent seulement si la capture est volontaire. Par exemple, si un moscovites décide d'aller entre deux soldats suédois, il ne sera pas capturé.");

			System.out.println("Merci d'avoir pris le temps de lire les rêgles." + "\n");
		}
	}

	/**
	*	La méthode afficherGagner() permet d'afficher les rêgles du jeu aux joueurs
	*	@param choix permet d'afficher soit la victoire des Moscovites soit la victoire des Suédois
	**/

	public void afficherGagner(int choix)
	{
		clearScreen();
		System.out.println(" ____  ____   _____     _____    _ ");
		System.out.println("| __ )|  _ \\ / _ \\ \\   / / _ \\  | |");
		System.out.println("|  _ \\| |_) | |_| \\ \\ / / | | | | |");
		System.out.println("| |_) |  _ <|  _  |\\ V /| |_| | |_|");
		System.out.println("|____/|_| \\_\\_| |_| \\_/  \\___/  (_)");
		if(choix == 1)
		{
			System.out.println("  ___  _   _    ____  ___  __  __ ____    ____  _____ ____    __  __  ___  ____   ____ _____     _____ _____ _____ ____  ");
			System.out.println(" / _ \\| | | |  / ___|/ _ \\|  \\/  |  _ \\  |  _ \\| ____/ ___|  |  \\/  |/ _ \\/ ___| / ___/ _ \\ \\   / /_ _|_   _| ____/ ___| ");
			System.out.println("| |_| | | | | | |   | |_| | |\\/| | |_) | | | | |  _| \\___ \\  | |\\/| | | | \\___ \\| |  | | | \\ \\ / / | |  | | |  _| \\___ \\ ");
			System.out.println("|  _  | |_| | | |___|  _  | |  | |  __/  | |_| | |___ ___) | | |  | | |_| |___) | |___ |_| |\\ V /  | |  | | | |___ ___) |");
			System.out.println("|_| |_|\\___/   \\____|_| |_|_|  |_|_|     |____/|_____|____/  |_|  |_|\\___/|____/ \\____\\___/  \\_/  |___| |_| |_____|____/ ");
		}
		else if(choix == 2)
		{
			System.out.println("  ___  _   _    ____  ___  __  __ ____    ____  _____ ____    ____  _   _ _____ ____   ___ ___ ____  ");
			System.out.println(" / _ \\| | | |  / ___|/ _ \\|  \\/  |  _ \\  |  _ \\| ____/ ___|  / ___|| | | | ____|  _ \\ / _ \\_ _/ ___| ");
			System.out.println("| |_| | | | | | |   | |_| | |\\/| | |_) | | | | |  _| \\___ \\  \\___ \\| | | |  _| | | | | | | | |\\___ \\ ");
			System.out.println("|  _  | |_| | | |___|  _  | |  | |  __/  | |_| | |___ ___) |  ___) | |_| | |___| |_| | |_| | | ___) |");
			System.out.println("|_| |_|\\___/   \\____|_| |_|_|  |_|_|     |____/|_____|____/  |____/ \\___/|_____|____/ \\___/___|____/ ");
		}
	}

	/**
	*	La méthode clearScreen() permet d'éffacer les informations précedentes pour plus de lisibilité. Cela équivaut à un "ctrl + l".
	**/

	public static void clearScreen()
	{
	 	System.out.print("\033[H\033[2J");
	 	System.out.flush();
	}

	public static void afficherTexte(String texte, int couleur)
	{
		System.out.print("\033["+couleur+"m");
		System.out.println(texte);
		System.out.print("\033[0m");
	}
	/*
	0 initialiser
	30 Noir
	31 Rouge
	32 Vert
	33 Jaune
	34 Bleu
	35 Magenta
	36 Cyan
	37 Blanc
	*/

}
