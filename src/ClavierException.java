package clavier;

/**
* Cette classe est la classe ClavierException
*
* Ecrit par Kévin Migadel
* @author Kévin Migadel
* @version 1.0
**/

public class ClavierException extends Exception {

	/**
  	* Le constructeur ClavierException permet d'afficher une exception il fait appelle à sa super classe Exception
	* @param message permet d'afficher le message de l'exception
	**/

	public ClavierException(String message) {
	    super(message);
	}

	/**
  	* Le constructeur ClavierException permet d'afficher une exception.
	* Il demande à l'utilisateur de re-saisir ce qu'il voulait écrire
	**/

  	public ClavierException() {
    	super("Re-saisir");
  	}
}
