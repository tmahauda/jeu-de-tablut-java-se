package plateau;
import pion.*;
import jeu.*;

/**
* Cette classe est la classe Plateau
*
* Ecrit par Kévin Migadel
* @author Kévin Migadel
* @version 1.0
**/

public class Plateau {

	private Case plateau[][];
	private Pion pions[];
	private boolean ggmosco;
	private boolean ggsuedois;

	/**
  	* Le constructeur Plateau permet de gérer le plateau
	**/

	public Plateau() {
		this.initialiserPlateau();
	}

	/**
	*	La méthode initialiserPlateau() permet d'initialiser le plateau
	**/

	public void initialiserPlateau() {
		this.plateau = new Case[10][10];
		this.pions = new Pion[25];
		ggsuedois = false;
		ggmosco = false;

		this.initialiserCase();
		this.initialiserPion();
	}

	/**
	*	La méthode initialiserCase() permet d'initialiser les cases du plateau
	**/

	public void initialiserCase() {
		for (int j = 0; j < 10; j++) {
	  		for (int i = 0; i < 10; i++) {
	        	this.plateau[i][j] = new Case(i, j);
	      	}
   		}
	}

	/**
	*	La méthode initialiserPion() permet d'initialiser les pions du plateau
	**/

	public void initialiserPion() {
		int compteur = 0;
		for (int i=0; i<10; i++) {
			for (int y=0; y<10; y++) {
				if (this.plateau[i][y].getCouleur() == 1) {
					this.pions[compteur] = new Soldat(2, "vert", "Suedois", this.plateau[i][y]);
					this.plateau[i][y].setEtat(1);
					compteur++;
				}
				else if (this.plateau[i][y].getCouleur() == 2) {
					this.pions[compteur] = new Moscovites(1, "jaune", "Moscovites", this.plateau[i][y]);
					this.plateau[i][y].setEtat(2);
					compteur++;
				}
				else if (this.plateau[i][y].getCouleur() == 3) {
					this.pions[compteur] = new Roi(3, "vert", "Suedois", this.plateau[i][y]);
					this.plateau[i][y].setEtat(3);
					compteur++;
				}
			}
		}
	}

	public void setEmplacement(Pion pion, Case cases) {
		// TODO
	}

	/**
	*	La méthode verifierDeplacement() permet de vérifier les déplacements d'un pion
	*	@param pion représente un pion
	*	@param cases représente une case
	*	@return Retourne vrai si le déplacement s'est bien déroulé ou faux s'il ne s'est pas bien déroulé
	*	@throws DeplacementException si la case est déjà occupée
	*	@throws DeplacementException si le pion s'arrête sur le trône (4,4)
	*	@throws DeplacementException si le pion veut traverser un autre pion qui se trouve sur le chemin
	*	@throws DeplacementException si le pion veut traverser le trône (4,4)
	*	@throws DeplacementException si une erreur innatendue survient
	**/

	public boolean verifierDeplacement(Pion pion, Case cases) throws DeplacementException{
		if(pion == null || cases == null) return false;
		int x1 = pion.getCase().getX();
		int y1 = pion.getCase().getY();
		int x2 = cases.getX();
		int y2 = cases.getY();
		try{
		if (pion.verifierDeplacementPion(x1, y1, x2, y2)) {
			if (x1 == x2) {
				if (y1-y2 >0) { //deplacement en haut
					if (cases.getEtat() != 0) {//Si la case est occupée
						throw new DeplacementException ("La case est occupée, vous n'allez pas voler la place de quelqu'un tout de même !");
					}
					else if(!(pion instanceof Roi) && cases.getCouleur() == 3){//Si le pion essaie de s'arreter sur le trone
							throw new DeplacementException ("Le pion ne peut pas s'arrêter sur le trône. PERSONNE D'AUTRE QUE LE ROI N'A LE DROIT DE TOUCHER LE TRÔNE");
							
					}
					else{
						for (int i=1; i< y1-y2; i++) {
							if (this.plateau[x1][y1-i].getEtat() != 0) {//Si la voie n'est pas libre
								throw new DeplacementException ("Un pion ne peut pas traverser d'autres pions, vous n'êtes pas Superman quand même ?");
								
							}
							if (this.plateau[x1][y1-i].getCouleur() == 3 && !(pion instanceof Roi) ) {//Si le trône est sur la voie
								throw new DeplacementException ("Un pion ne peut pas traverser le trône. PERSONNE D'AUTRE QUE LE ROI N'A LE DROIT DE TOUCHER LE TRÔNE");
								
							}
						}

						pion.getCase().setEtat(0);
						pion.setCase(cases);
						if (pion instanceof Moscovites) {
							pion.getCase().setEtat(2);
						}
						else if (pion instanceof Soldat) {
							pion.getCase().setEtat(1);
						}
						else if (pion instanceof Roi) {
							pion.getCase().setEtat(3);
							this.verifierGagner(pion);
						}
						else{
							throw new DeplacementException ("Erreur innatendue, veuillez contatacter le support");
							
						}
						return true;
					}

				}
				else if(y1-y2 <0){ //deplacement en bas
					if (cases.getEtat() != 0) {//Si la case est occupée
						throw new DeplacementException ("La case est occupée, vous n'allez pas voler la place de quelqu'un tout de même !");
						
					}
					else if(!(pion instanceof Roi)  && cases.getCouleur() == 3){//Si le pion essaie de s'arreter sur le trone
							throw new DeplacementException ("Le pion ne peut pas s'arrêter sur le trône. PERSONNE D'AUTRE QUE LE ROI N'A LE DROIT DE TOUCHER LE TRÔNE");
							
					}
					else{
						for (int i=1; i< y2-y1; i++) {
							if (this.plateau[x1][y1+i].getEtat() != 0) {//Si la voie n'est pas libre
								throw new DeplacementException ("Un pion ne peut pas traverser d'autres pions, vous n'êtes pas Superman quand même ?");
								
							}
							if (this.plateau[x1][y1+i].getCouleur() == 3 && !(pion instanceof Roi) ) {//Si le trône est sur la voie
								throw new DeplacementException ("Un pion ne peut pas traverser le trône. PERSONNE D'AUTRE QUE LE ROI N'A LE DROIT DE TOUCHER LE TRÔNE");
								
							}
						}
						pion.getCase().setEtat(0);
						pion.setCase(cases);
						if (pion instanceof Moscovites) {
							pion.getCase().setEtat(2);
						}
						else if (pion instanceof Soldat) {
							pion.getCase().setEtat(1);
						}
						else if (pion instanceof Roi) {
							pion.getCase().setEtat(3);
							this.verifierGagner(pion);
						}
						else{
							throw new DeplacementException ("Erreur innatendue, veuillez contatacter le support");
							
						}
						return true;
					}
				}
				else{
					throw new DeplacementException ("Erreur innatendue, veuillez contacter le support");
					
				}

			}
			else if(y1 == y2){
				if (x1-x2 >0) { //deplacement à gauche
					if (cases.getEtat() != 0) {//Si la case est occupée
						throw new DeplacementException ("La case est occupée, vous n'allez pas voler la place de quelqu'un tout de même !");
						
					}
					else if(!(pion instanceof Roi)  && cases.getCouleur() == 3){//Si le pion essaie de s'arreter sur le trone
							throw new DeplacementException ("Le pion ne peut pas s'arrêter sur le trône. PERSONNE D'AUTRE QUE LE ROI N'A LE DROIT DE TOUCHER LE TRÔNE");
							
					}
					else{
						for (int i=1; i< x1-x2; i++) {//Si la voie n'est pas libre
							if (this.plateau[x1-i][y1].getEtat() != 0) {
								throw new DeplacementException ("Un pion ne peut pas traverser d'autres pions, vous n'êtes pas Superman quand même ?");
								
							}
							if (this.plateau[x1-i][y1].getCouleur() == 3 && !(pion instanceof Roi) ) {//Si le trône est sur la voie
								throw new DeplacementException ("Un pion ne peut pas traverser le trône. PERSONNE D'AUTRE QUE LE ROI N'A LE DROIT DE TOUCHER LE TRÔNE");
								
							}
						}
						pion.getCase().setEtat(0);
						pion.setCase(cases);
						if (pion instanceof Moscovites) {
							pion.getCase().setEtat(2);
						}
						else if (pion instanceof Soldat) {
							pion.getCase().setEtat(1);
						}
						else if (pion instanceof Roi) {
							pion.getCase().setEtat(3);
							this.verifierGagner(pion);
						}
						return true;
					}
				}
				else if(x1-x2 <0){ //deplacement à droite
					if (cases.getEtat() != 0) {//Si la case est occupée
						throw new DeplacementException ("La case est occupée, vous n'allez pas voler la place de quelqu'un tout de même !");
						
					}
					else if(!(pion instanceof Roi)  && cases.getCouleur() == 3){//Si le pion essaie de s'arreter sur le trone
							throw new DeplacementException ("Le pion ne peut pas s'arrêter sur le trône. PERSONNE D'AUTRE QUE LE ROI N'A LE DROIT DE TOUCHER LE TRÔNE");
							
					}
					else{
						for (int i=1; i< x2-x1; i++) {
							if (this.plateau[x1+i][y1].getEtat() != 0) {//Si la voie n'est pas libre
								throw new DeplacementException ("Un pion ne peut pas traverser d'autres pions, vous n'êtes pas Superman quand même ?");
								
							}
							if (this.plateau[x1+i][y1].getCouleur() == 3 && !(pion instanceof Roi) ) {//Si le trône est sur la voie
								throw new DeplacementException ("Un pion ne peut pas traverser le trône. PERSONNE D'AUTRE QUE LE ROI N'A LE DROIT DE TOUCHER LE TRÔNE");
								
							}
						}
						pion.getCase().setEtat(0);
						pion.setCase(cases);
						if (pion instanceof Moscovites) {
							pion.getCase().setEtat(2);
						}
						else if (pion instanceof Soldat) {
							pion.getCase().setEtat(1);
						}
						else if (pion instanceof Roi) {
							pion.getCase().setEtat(3);
							this.verifierGagner(pion);
						}
						return true;
					}
				}
				else{
					throw new DeplacementException ("Erreur innatendue, veuillez contatacter le support");
					
				}

			}
			else{
				throw new DeplacementException ("Erreur innatendue, veuillez contacter le support");
				
			}
			}
		}
		catch(DeplacementException e){
			throw new DeplacementException (e.getMessage());
		}
			return false;
	}

	/**
	*	La méthode verifierCapturer() permet de vérifier si un pion à été capturé
	*	@param pion représente un pion
	*	@return Retourne vrai si un pion s'est a été capturé ou faux s'il ne s'est pas fait capturé
	**/

	public boolean verifierCapturer(Pion pion) {
		int x1 = pion.getCase().getX();
		int y1 = pion.getCase().getY();
		boolean capture = false;

		if (y1 >= 2 && this.plateau[x1][y1-1].getEtat() != 0) { //Haut

				if (pion.getCamp() == "Suedois" && this.plateau[x1][y1-1].getEtat() == 2) {


						if (this.plateau[x1][y1-2].getEtat() == 1 || this.plateau[x1][y1-2].getEtat() == 3) {
							Pion pionCapture = null;
							boolean trouve = false;
							for (int i=0; i <25 && trouve == false; i++) {
								if (this.pions[i].getCase() != null) {
								if (this.pions[i].getCase().equals(this.plateau[x1][y1-1])) {
										trouve = true;
										pionCapture = this.pions[i];
										capture = true;
									}
								}
							}
							pionCapture.getCase().setEtat(0);
							pionCapture.setCase(null);
						}

				}

				else if (pion.getCamp() == "Moscovites" && this.plateau[x1][y1-1].getEtat() == 1) {


						if (this.plateau[x1][y1-2].getEtat() == 2) {
							Pion pionCapture = null;
							boolean trouve = false;
							for (int i=0; i <25 && trouve == false; i++) {
								if (this.pions[i].getCase() != null) {
								if (this.pions[i].getCase().equals(this.plateau[x1][y1-1])) {
										trouve = true;
										pionCapture = this.pions[i];
										capture = true;
									}
								}
							}
							pionCapture.getCase().setEtat(0);
							pionCapture.setCase(null);
						}


				}

				else if (pion.getCamp() == "Moscovites" && this.plateau[x1][y1-1].getEtat() == 3) {

					if ((this.plateau[x1][y1-2].getEtat() == 2 || this.plateau[x1][y1-2].getCouleur() == 3 || y1 == 1) 
						&& (this.plateau[x1-1][y1-1].getEtat() == 2 || this.plateau[x1-1][y1-1].getCouleur() == 3 || x1 == 0)
						&& (this.plateau[x1+1][y1-1].getEtat() == 2 || this.plateau[x1+1][y1-1].getCouleur() == 3 || x1 == 8)) {
							Pion pionCapture = null;
							boolean trouve = false;
							for (int i=0; i <25 && trouve == false; i++) {
								if (this.pions[i].getCase() != null) {
								if (this.pions[i] instanceof Roi) {
										trouve = true;
										pionCapture = this.pions[i];
										capture = true;
									}
								}
							}
							pionCapture.getCase().setEtat(0);
							pionCapture.setCase(null);
							this.setGgmosco(true); //TODO AND TO SEE ANEC THEO
						}

				}
		}

		if (y1 <= 7 && this.plateau[x1][y1+1].getEtat() != 0) { //Bas

			if (pion.getCamp() == "Suedois" && this.plateau[x1][y1+1].getEtat() == 2) {


						if (this.plateau[x1][y1+2].getEtat() == 1 || this.plateau[x1][y1+2].getEtat() == 3) {
							Pion pionCapture = null;
							boolean trouve = false;
							for (int i=0; i <25 && trouve == false; i++) {
								if (this.pions[i].getCase() != null) {
								if (this.pions[i].getCase().equals(this.plateau[x1][y1+1])) {
										trouve = true;
										pionCapture = this.pions[i];
										capture = true;
									}
								}
							}
							pionCapture.getCase().setEtat(0);
							pionCapture.setCase(null);
						}

				}

				else if (pion.getCamp() == "Moscovites" && this.plateau[x1][y1+1].getEtat() == 1) {


						if (this.plateau[x1][y1+2].getEtat() == 2) {
							Pion pionCapture = null;
							boolean trouve = false;
							for (int i=0; i <25 && trouve == false; i++) {
								if (this.pions[i].getCase() != null) {
								if (this.pions[i].getCase().equals(this.plateau[x1][y1+1])) {
										trouve = true;
										pionCapture = this.pions[i];
										capture = true;
									}
								}
							}
							pionCapture.getCase().setEtat(0);
							pionCapture.setCase(null);
						}


				}

				else if (pion.getCamp() == "Moscovites" && this.plateau[x1][y1+1].getEtat() == 3) {

					if ((this.plateau[x1][y1+2].getEtat() == 2 || this.plateau[x1][y1+2].getCouleur() == 3 || y1 == 7)
						 && (this.plateau[x1-1][y1+1].getEtat() == 2 || this.plateau[x1-1][y1+1].getCouleur() == 3 || x1 == 0)
						 && (this.plateau[x1+1][y1+1].getEtat() == 2 || this.plateau[x1+1][y1+1].getCouleur() == 3 || x1 == 8)) {
							Pion pionCapture = null;
							boolean trouve = false;
							for (int i=0; i <25 && trouve == false; i++) {
								if (this.pions[i].getCase() != null) {
								if (this.pions[i] instanceof Roi) {
										trouve = true;
										pionCapture = this.pions[i];
										capture = true;
									}
								}
							}
							pionCapture.getCase().setEtat(0);
							pionCapture.setCase(null);
							this.setGgmosco(true); //TODO AND TO SEE ANEC THEO
						}

				}

		}

		if (x1 >= 2 && this.plateau[x1-1][y1].getEtat() != 0) { //Gauche

			if (pion.getCamp() == "Suedois" && this.plateau[x1-1][y1].getEtat() == 2) {


						if (this.plateau[x1-2][y1].getEtat() == 1 || this.plateau[x1-2][y1].getEtat() == 3) {
							Pion pionCapture = null;
							boolean trouve = false;
							for (int i=0; i <25 && trouve == false; i++) {
								if (this.pions[i].getCase() != null) {
								if (this.pions[i].getCase().equals(this.plateau[x1-1][y1])) {
										trouve = true;
										pionCapture = this.pions[i];
										capture = true;
									}
								}
							}
							pionCapture.getCase().setEtat(0);
							pionCapture.setCase(null);
						}

				}

				else if (pion.getCamp() == "Moscovites" && this.plateau[x1-1][y1].getEtat() == 1) {


						if (this.plateau[x1-2][y1].getEtat() == 2) {
							Pion pionCapture = null;
							boolean trouve = false;
							for (int i=0; i <25 && trouve == false; i++) {
								if (this.pions[i].getCase() != null) {
								if (this.pions[i].getCase().equals(this.plateau[x1-1][y1])) {
										trouve = true;
										pionCapture = this.pions[i];
										capture = true;
									}
								}
							}
							pionCapture.getCase().setEtat(0);
							pionCapture.setCase(null);
						}


				}

				else if (pion.getCamp() == "Moscovites" && this.plateau[x1-1][y1].getEtat() == 3) {

					if ((this.plateau[x1-2][y1].getEtat() == 2 || this.plateau[x1-2][y1].getCouleur() == 3 || x1 == 1) 
							&& (this.plateau[x1-1][y1+1].getEtat() == 2 || this.plateau[x1-1][y1+1].getCouleur() == 3 || y1 == 8) 
							&& (this.plateau[x1-1][y1-1].getEtat() == 2 || this.plateau[x1-1][y1-1].getCouleur() == 3 || y1 == 0)) {
							Pion pionCapture = null;
							boolean trouve = false;
							for (int i=0; i <25 && trouve == false; i++) {
								if (this.pions[i].getCase() != null) {
								if (this.pions[i] instanceof Roi) {
										trouve = true;
										pionCapture = this.pions[i];
										capture = true;
									}
								}
							}
							pionCapture.getCase().setEtat(0);
							pionCapture.setCase(null);
							this.setGgmosco(true); //TODO AND TO SEE ANEC THEO
						}

				}

		}

		if (x1 <=7 && this.plateau[x1+1][y1].getEtat() != 0) { //Droite

			if (pion.getCamp() == "Suedois" && this.plateau[x1+1][y1].getEtat() == 2) {


						if (this.plateau[x1+2][y1].getEtat() == 1 || this.plateau[x1+2][y1].getEtat() == 3) {
							Pion pionCapture = null;
							boolean trouve = false;
							for (int i=0; i <25 && trouve == false; i++) {
								if (this.pions[i].getCase() != null) {
								if (this.pions[i].getCase().equals(this.plateau[x1+1][y1])) {
										trouve = true;
										pionCapture = this.pions[i];
										capture = true;
									}
								}
							}
							pionCapture.getCase().setEtat(0);
							pionCapture.setCase(null);
						}

				}

				else if (pion.getCamp() == "Moscovites" && this.plateau[x1+1][y1].getEtat() == 1) {


						if (this.plateau[x1+2][y1].getEtat() == 2) {
							Pion pionCapture = null;
							boolean trouve = false;
							for (int i=0; i <25 && trouve == false; i++) {
								if (this.pions[i].getCase() != null) {
								if (this.pions[i].getCase().equals(this.plateau[x1+1][y1])) {
										trouve = true;
										pionCapture = this.pions[i];
										capture = true;
									}
								}
							}
							pionCapture.getCase().setEtat(0);
							pionCapture.setCase(null);
						}


				}

				else if (pion.getCamp() == "Moscovites" && this.plateau[x1+1][y1].getEtat() == 3) {

					if ((this.plateau[x1+2][y1].getEtat() == 2 || this.plateau[x1+2][y1].getCouleur() == 3 || x1 == 7) 
							&& (this.plateau[x1+1][y1+1].getEtat() == 2 || this.plateau[x1+1][y1+1].getCouleur() == 3 || y1 == 8)
							&& (this.plateau[x1+1][y1-1].getEtat() == 2 || this.plateau[x1+1][y1-1].getCouleur() == 3 || y1 == 0)) {
							Pion pionCapture = null;
							boolean trouve = false;
							for (int i=0; i < 25 && trouve == false; i++) {
								if (this.pions[i].getCase() != null) {
								if (this.pions[i] instanceof Roi) {
										trouve = true;
										pionCapture = this.pions[i];
										capture = true;
									}
								}
							}
							pionCapture.getCase().setEtat(0);
							pionCapture.setCase(null);
							this.setGgmosco(true); //TODO AND TO SEE ANEC THEO
						}

				}

		}
		return capture;
	}

	public void supprimerPion(Pion pioncapture, Case cases) {
		// TODO
	}

	/**
	*	La méthode verifierGagner() permet de vérifier si le Suédois ont gagnés, c.a.d le Roi est sur un des côtés du plateau, mais sur les cases roses
	*	@param roi représente le Roi
	**/

	public void verifierGagner(Pion roi) {
		this.ggsuedois = ((roi.getCase().getX() == 0 || roi.getCase().getX() == 8 || roi.getCase().getY() == 0 ||roi.getCase().getY() == 8) && (roi.getCase().getCouleur() != 2));
	}

	/**
	*	La méthode getCase() permet d'afficher une case du plateau
	*	@param x reprèsente les abscisses
	*	@param y reprèsente les ordonnées
	*	@return Retourne la case correspondante à l'abscisse et à l'ordonnée rentrées en paramètre
	**/

	public Case getCase(int x, int y) {
		return this.plateau[x][y];
	}

	/**
	*	La méthode getPion() permet d'afficher un pion 
	*	@param cases reprèsente une case
	*	@param joueur reprèsente un joueur
	*	@return Retourne un pion ou une exception
	*	@throws DeplacementException si la case ne contient pas de pion
	*	@throws DeplacementException si le pion n'appartient pas au camp du joueur mis en paramètre
	**/

	public Pion getPion(Case cases, Joueur joueur) throws DeplacementException
	{
		for (int i=0; i <25; i++)
		{
			if (this.pions[i].getCase() != null) {
				
				if (this.pions[i].getCase().equals(cases))
				{
					if(this.pions[i].getCamp().equals(joueur.getCamp())) return this.pions[i];
					else throw new DeplacementException("Le pion sélectionné appartient au camp adverse");
				}
				
			}
		}
		throw new DeplacementException("Le pion situé à la case "+cases.toString()+" n'existe pas");
	}

	/**
	*	La méthode getGgmosco() permet de savoir si les Moscovites ont gagnés
	*	@return Retourne vrai si les Moscovites ont gagnés ou faux s'ils n'ont pas encore gagnés
	**/

	public boolean getGgmosco(){
		return ggmosco;
	}

	/**
	*	La méthode getGgsuedois() permet de savoir si les Suédois ont gagnés
	*	@return Retourne vrai si les Suédois ont gagnés ou faux s'ils n'ont pas encore gagnés
	**/

	public boolean getGgsuedois(){
		return ggsuedois;
	}

	/**
	*	La méthode setGgmosco() permet de définir si les Moscovites ont gagnés
	*	@param b est un boolean qui détermine si les Moscovites ont gagnés
	**/

	public void setGgmosco(boolean b){
		this.ggmosco = b;
	}

	/**
	*	La méthode setGgsuedois() permet de définir si les Suédois ont gagnés
	*	@param b est un boolean qui détermine si les Suédois ont gagnés
	**/

	public void setGgsuedois(boolean b){
		this.ggsuedois = b;
	}

	/**
	*	La méthode getGg() permet de savoir si un des deux camp à gagnés
	*	@return Retourne vrai si les Suédois ou les Moscovites ont gagnés ou faux s'ils n'ont pas encore gagnés
	**/

	public boolean getGg(){
		return ggsuedois || ggmosco;
	}

	/**
	*	La méthode getPions() permet d'afficher un pion
	*	@return Retourne un pion
	**/

	public Pion[] getPions(){
		return this.pions;
	}

}
