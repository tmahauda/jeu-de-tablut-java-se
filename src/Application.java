import jeu.*;
import clavier.*;

class Application {
  public static void main(String[] args)
  {
    Clavier clavier = new Clavier();
    System.out.print("\033[0m");
    System.out.print("\033[H\033[2J");
    System.out.flush();
    System.out.println("Pour profiter au mieux de l'expérience utilisateur, veuillez afficher le terminal en plein écran");
    System.out.println("Lorsque vous l'aurez fait, taper 1 pour continuer.");
    try
    {
      if(clavier.demanderChoix())
      {
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                System.out.println();
                System.out.println("\nMerci d'avoir joué au jeu du Tablut. A bientôt");
            }
        }));
        new Partie();
      }
    }
    catch(ClavierException e)
    {
      System.out.println("Choix non reconnu. Réessayer de lancer le jeu");
      System.exit(1);
    }
  }
}
