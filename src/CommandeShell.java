package shell;
import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
* Cette classe est la classe CommandeShell
*
* Ecrit par Kévin Migadel
* @author Kévin Migadel
* @version 1.0
**/

public class CommandeShell {

	public CommandeShell(String cmd)
	{
		try
		{
			Runtime run = Runtime.getRuntime();
			Process pr = run.exec(cmd);
			pr.waitFor();
			BufferedReader buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
			String line = "";
			while ((line=buf.readLine())!=null)
			{
				System.out.println(line);
			}
		}
		catch(Exception e){}
	}
}
