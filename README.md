# Jeu de Tablut

<div align="center">
<img width="500" height="400" src="Tablut.png">
</div>

## Description du projet

Application console réalisée avec Java SE en DUT INFO 1 à l'IUT de Nantes dans le cadre du module "Projet tutoré - Description et planification de projet" durant l'année 2016-2017 avec un groupe de cinq personnes. \
Elle consiste à jouer au jeu de Tablut sur son ordinateur depuis un terminal avec une personne de son choix pour passer le temps en s’amusant ! \
Le jeu de Tablut est décrit ici: http://jeuxstrategieter.free.fr/Tablut_complet.php.

## Acteurs

### Réalisateurs

Ce projet a été réalisé par un groupe de cinq étudiants de l'IUT de Nantes :
- Théo MAHAUDA : theo.mahauda@etu.univ-nantes.fr ;
- Kévin MIGADEL : kevin.migadel@etu.univ-nantes.fr ;
- Rémy VOILLET : remy.voillet@etu.univ-nantes.fr ; 
- Axel BROCHARD : axel.brochard@etu.univ-nantes.fr ;
- Tanguy CORNEC : tanguy.cornec@etu.univ-nantes.fr.

### Encadrants

Ce projet fut encadré par deux enseignants de l'IUT de Nantes :
- Christine JACQUIN : jacquin.christine@etu.univ-nantes.fr ;
- Saïd EL MAMOUNI : said.el-mamouni@etu.univ-nantes.fr.

## Organisation

Ce projet a été agit au sein de l'université de Nantes dans le cadre du module "Projet tutoré - Description et planification de projet" du DUT INFO 1.

## Date de réalisation

Ce projet a été éxécuté durant l'année 2017 sur une durée de six mois environs (période du second semestre) en parallèle des cours. \
Il a été terminé et rendu le 09/04/2017.

## Technologies, outils et procédés utilisés

Ce projet a été accomplis avec les technologies, outils et procédés suivants :
- Java ;
- Eclipse ;
- Diagrammes UML (cas d'utilisation, séquence, classe) ;
- POO.

## Objectifs

La finalité de ce projet est de pouvoir jouer au jeu du Tablut sur son ordinateur avec un partenaire à ses côtés depuis une interface. \
Les joueurs peuvent, après avoir choisis leur pseudonyme et leur camp, jouer une partie entière, c’est à dire deux manches sans qu’il n’y ait de problème. \
De plus un bouton aide devra être présent sur le jeu qui devra conseiller le joueur sur ce qu’il peut faire au niveau du coup à effectuer s’il est bloqué. \
Néanmoins chaque joueur est limité à une seule aide durant chaque manche.